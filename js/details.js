$(document).ready(function () {  
var my_url = location.search.replace('?', '').split('=');  
var id = my_url[1];

var cadena = $.getJSON("propiedades.json", function (data) {
  var count= data.propiedades.property.length;
    for(var i=0; i < count ; i++) {
      if(data.propiedades.property[i].id == id ){
        var imgSmall= "";
        var  arrayImg=[];

        var price = data.propiedades.property[i].price;
        var operation = data.propiedades.property[i].type_property + ' en ' +
                        data.propiedades.property[i].type_operation;
      
        var address="";
        var state="";
        for(var direccion in  data.propiedades.property[i].address) {
          address = data.propiedades.property[i].address.street
                                            + ' # ' + data.propiedades.property[i].address.number_ext
                                            + ' Int. ' + data.propiedades.property[i].address.number_int;          
          state = data.propiedades.property[i].address.city
                                            + ', ' + data.propiedades.property[i].address.state;
        }

        var neighborhood = data.propiedades.property[i].neighborhood+', ';


        $.each(data.propiedades.property[i].gallery, function (ind, elem) { 
          arrayImg[ind] = elem;          
          imgSmall += "<img id='img"+ind+"' class='imgSec' src='../img/"+ elem+"' alt='img'>"    
          $(".thumb").html(imgSmall);          
          
          if(ind==1) {
            var background= "<div id='divImgPrincipal' style='background-image: url(../img/"+elem+");"+
                                                             "background-size: cover !important; "+
                                                             "background-position: center center; " +
                                                             "background-repeat: no-repeat !important;padding: 30% 2% 2% 2%; '>"+
                                                             "<h5 class='text-white font-weight-bold'>"+operation+"</h5>"+
                                                             "<h3 class='text-white font-weight-bold'>"+address+"</h3>"+
                                                             "<h5 class='text-white font-weight-bold'>"+neighborhood+state+"</h5>"+
                                                             "<h3 class='text-success font-weight-bold'>"+price+"</h3>"+"</div>";
            
            $("#divImgPrincipal").html(background);            
          }
        }); 

        $('img').removeClass("activo");
        $("#img1").addClass("activo");  

        var current=1;        
        $("#btnNext").on("click", function() {          
          var next= parseInt(current) + 1;                        
          if(next <=10) {                    
            var background= "<div id='divImgPrincipal' style='background-image: url(../img/"+arrayImg[next]+");"+
                                                             "background-size: cover !important; "+
                                                             "background-position: center center; " +
                                                             "background-repeat: no-repeat !important;padding: 30% 2% 2% 2%; '>"+
                                                             "<h5 class='text-white font-weight-bold'>"+operation+"</h5>"+
                                                             "<h3 class='text-white font-weight-bold'>"+address+"</h3>"+
                                                             "<h5 class='text-white font-weight-bold'>"+neighborhood+state+"</h5>"+
                                                             "<h3 class='text-success font-weight-bold'>"+price+"</h3>"+"</div>";
            
            $("#divImgPrincipal").html(background);
            $('img').removeClass("activo");
            $("#img"+next).addClass("activo");
            current= next;
          }          
        });  

        var retroceso=0;
        $("#btnPrev").on("click", function() {
          retroceso++;
          var prev = current - retroceso;               
          if(prev > 0 && prev <=10){                      
            var background= "<div id='divImgPrincipal' style='background-image: url(../img/"+arrayImg[prev]+");"+
                                                             "background-size: cover !important; "+
                                                             "background-position: center center; " +
                                                             "background-repeat: no-repeat !important;padding: 30% 2% 2% 2%; '>"+
                                                             "<h5 class='text-white font-weight-bold'>"+operation+"</h5>"+
                                                             "<h3 class='text-white font-weight-bold'>"+address+"</h3>"+
                                                             "<h5 class='text-white font-weight-bold'>"+neighborhood+state+"</h5>"+
                                                             "<h3 class='text-success font-weight-bold'>"+price+"</h3>"+"</div>";
            
            $("#divImgPrincipal").html(background);
            $('img').removeClass("activo");
            $("#img"+prev).addClass("activo");
          }
          prev = 1;         
        });


        $(".address").html('Detales de ' + address);
        $(".price").html(price);
        $(".operation").html(operation);        
        $(".description").html(data.propiedades.property[i].description);

      }
    }

});

});

