$(document).ready(function () {        
  var contenido= "";
  var cadena = $.getJSON("propiedades.json", function (data) {
    var count= data.propiedades.property.length;
    for(var i=0; i < count ; i++) {
      $(".photo").prop('src','../img/'+data.propiedades.property[i].photo);
      $(".price").html(data.propiedades.property[i].price);
      $(".operation").html(data.propiedades.property[i].type_operation + ' de ' +
                           data.propiedades.property[i].type_property  + ' en ' +
                           data.propiedades.property[i].neighborhood);
      $(".description").html(data.propiedades.property[i].description);

      $(".green-button").prop('href','details.html?id='+data.propiedades.property[i].id);

      for(var direccion in  data.propiedades.property[i].address) {
        $(".street").html(data.propiedades.property[i].address.street);
        $(".number").html(' # '   + data.propiedades.property[i].address.number_ext +
                          ' Int. '+ data.propiedades.property[i].address.number_int);
      }
      contenido += $("#item").html();
      $("#padre").html(contenido); 
    }
  
  });
});

